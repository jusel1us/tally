class Counter {
    constructor() {
        this.count = 0;
    }

    increase() {
        this.count++;
    }

    zero() {
        this.count = 0;
    }

    read() {
        return this.count;
    }
}

export default Counter;