import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    // Log every request with its HTTP method and URL.
    logger.info(`[ENDPOINT] ${req.method} '${req.path}'`);
    next();
};

export default endpoint_mw;
