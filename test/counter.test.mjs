import { describe, it} from "mocha";
import { expect } from 'chai';
import Counter from "../src/counter.mjs";

let counter = new Counter();

describe('Counter tests', () => {
    it('Initial counter count is 0', () => {
        counter = new Counter();
        expect(counter.count).to.equal(0);
    });
    it('Counter count can be increased', () => {
        counter = new Counter();
        counter.increase();
        expect(counter.count).to.equal(1);
    });
    it('Counter count can be increased and zeroed', () => {
        counter = new Counter();
        counter.increase();
        expect(counter.read()).to.equal(1);
        counter.zero();
        expect(counter.count).to.equal(0);
    });
});

import routes from "../src/router.mjs";

/** @type {import('node:http').Server} */
let server = undefined;

const base_url = "http://127.0.0.1:3000";

const waitSeconds = async (seconds) => new Promise((resolve) => {
    setTimeout(() => resolve(), (1000 * seconds));
});

describe('Routes', () => {
    before((done) => {
        server = routes.listen(3000, '127.0.0.1', () => done());
    });
    it('Can get welcome message', () => {
        fetch(base_url)
            .then(response => {
                expect(response.status).to.equal(200, "Incorrect status code");
                return response.text();
            })
            .then(text => expect(text).to.equal('Welcome!'))
            .catch(err => {
                console.error(err);
            });
    });
    it('Counter REST API works as expected', async () => {
        const r1 = await fetch(`${base_url}/counter-read`);
        expect(r1.status).to.equal(200, "Incorrect status code");
        expect(await r1.text()).to.equal('0', "Initial reading /counter-read");

        const r2 = await fetch(`${base_url}/counter-increase`);
        expect(r2.status).to.equal(200, "Incorrect status code");
        expect(await r2.text()).to.equal('1', "First increase /counter-increase");
        
        const r3 = await fetch(`${base_url}/counter-increase`);
        expect(r3.status).to.equal(200, "Incorrect status code");
        expect(await r3.text()).to.equal('2', "Second increase /counter-increase");
        
        const r4 = await fetch(`${base_url}/counter-zero`);
        expect(r4.status).to.equal(200, "Incorrect status code");
        expect(await r4.text()).to.equal('0', "Counter zeroing /counter-zero");
    });

    after(() => {
        server.close();
    });
});