import { describe, it, after } from 'mocha';
import { expect } from 'chai';
import fs from 'fs';

import routes from "../src/router.mjs";

// const levels = [ "emerg", "alert", "crit", "error", "warn", "notice", "info", "debug" ];
const levels = [ "error", "warn", "info", "http", "verbose", "debug", "silly" ];
const messages = ["[ENDPOINT] GET '/non-existing'", "[ENDPOINT] POST '/non-existing'", "[ENDPOINT] GET '/counter-read'"];

const waitSeconds = async (seconds) => new Promise((resolve) => {
    setTimeout(() => resolve(), (1000 * seconds));
});

const parseJSONLFile = async (filePath) => {
    /** @type {Array<any>} */
    const jsons = [];
    try {
        const fileContent = await fs.promises.readFile(filePath, 'utf-8');
        const rows = fileContent.split('\n').filter(line => line.trim() !== '');
        for (const row of rows) {
            const _json = JSON.parse(row);
            jsons.push(_json);
        }
    } catch (error) {
        console.error('Error reading or parsing JSONL file: ', error);
    }
    return jsons;
};

/**
 * @typedef Log
 * @property {string} level
 * @property {string} message
 * @property {string} timestamp
 */

/** @type {import('node:http').Server} */
let server = undefined;

const base_url = "http://127.0.0.1:3001";

describe('Testing logs', () => {
    const originalLogFunction = console.log;
    before((done) => {
        server = routes.listen(3001, '127.0.0.1', () => done());
    });
    let output = '';
    beforeEach(async () => {
        output = '';
        console.log = (msg) => {
            output += msg + '\n';
        };
    });
    it('Generates proper application logs', async () => {
        await waitSeconds(1);
        await fetch(`${base_url}/counter-read`);
        const r1 = await fetch(`${base_url}/non-existing`);
        expect(r1.status).to.equal(404, "Tested non-existing resource, but response status code was not 404.");
        // const r2 = await fetch(`${base_url}/non-existing`, {method: "POST"});
        // expect(r2.status).to.equal(404, "Tested to POST resource into /non-existing path, but response status code was not 404")
        /** @type {Array<Log>} */
        const combined_logs = await parseJSONLFile('logs/combined.log');
        expect(combined_logs.length).to.be.greaterThan(0);
        let hits = 0;
        for (const _log of combined_logs) {
            expect(_log.level).to.be.oneOf(levels);
            hits += messages.includes(_log.message) ? 1 : 0;
        }
        expect(hits).to.be.greaterThan(1);
    });
    after(() => {
        server.close();
    });
});